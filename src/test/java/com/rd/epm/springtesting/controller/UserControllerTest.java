package com.rd.epm.springtesting.controller;

import com.rd.epm.springtesting.model.User;
import com.rd.epm.springtesting.service.UserService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static com.rd.epm.springtesting.util.UserGenerator.generateUsers;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

public class UserControllerTest {

    private final UserService userService = Mockito.mock(UserService.class);
    private final UserController sut = new UserController(userService);
    private final MockMvc mockMvc = standaloneSetup(sut).build();


    @Test
    public void findAllUsers() throws Exception {
        List<User> userList = generateUsers(2);
        when(userService.findAll()).thenReturn(userList);

        mockMvc.perform(get("/users")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id", is(userList.get(0).getId())))
                .andExpect(jsonPath("$[1].id", is(userList.get(1).getId())))
                .andExpect(jsonPath("$[0].email", is(userList.get(0).getEmail())))
                .andExpect(jsonPath("$[1].email", is(userList.get(1).getEmail())));
    }

}
