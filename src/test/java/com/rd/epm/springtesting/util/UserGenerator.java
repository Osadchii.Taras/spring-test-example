package com.rd.epm.springtesting.util;

import com.rd.epm.springtesting.model.User;
import lombok.experimental.UtilityClass;

import java.util.ArrayList;
import java.util.List;

@UtilityClass
public class UserGenerator {

    public static List<User> generateUsers(Integer count) {
        List<User> userList = new ArrayList<>();
        for (int i = 1; i <= count; i++) {
            User user = new User();
            user.setId("RandomUUID");
            user.setEmail("user@email" + i);
            userList.add(user);
        }
        return userList;
    }
}
