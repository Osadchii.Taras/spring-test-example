package com.rd.epm.springtesting.service;

import com.rd.epm.springtesting.exception.UserNotFoundException;
import com.rd.epm.springtesting.model.User;
import com.rd.epm.springtesting.repository.UserRepository;
import config.AbstractIntegrationTest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static com.rd.epm.springtesting.util.UserGenerator.generateUsers;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;


public class UserServiceTestIT extends AbstractIntegrationTest {

    @Autowired
    private UserService sut;

    @Autowired
    private UserRepository repository;

    @Test
    public void shouldFindAllUsers() {
        //GIVEN
        repository.saveAll(generateUsers(5));
        //WHEN
        List<User> userList = sut.findAll();
        //THEN
        assertEquals(5, userList.size());
    }

    @Test
    public void shouldFindUserById() {
        //GIVEN
        User user = repository.save(generateUsers(1).get(0));
        //WHEN
        User expectedUser = sut.findById(user.getId());
        //THEN
        assertNotNull(expectedUser);
        assertEquals(expectedUser.getId(), user.getId());
    }

    @Test
    public void shouldThrowUserNotFoundException() {
        //GIVEN
        //WHEN
        //THEN
        assertThrows(UserNotFoundException.class, () -> sut.findById("incorrectId"));
    }

}
