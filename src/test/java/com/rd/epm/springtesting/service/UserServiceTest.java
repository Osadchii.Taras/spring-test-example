package com.rd.epm.springtesting.service;

import com.rd.epm.springtesting.model.User;
import com.rd.epm.springtesting.repository.UserRepository;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

//@ExtendWith({MockitoExtension.class})
public class UserServiceTest {

    private final UserRepository userRepository = mock(UserRepository.class);
    private final UserService sut = new UserServiceImpl(userRepository);

//    @Mock
//    private UserRepository userRepository;
//    @InjectMocks
//    private UserServiceImpl sut;


    @Test
    public void shouldFindUserById() {
        //GIVEN
        User mockUser = new User("superrandomID", "mock@user");
        when(userRepository.findById(any())).thenReturn(Optional.of(mockUser));
        //WHEN
        User user = sut.findById("superrandomID");
        //THEN
        verify(userRepository, times(1)).findById(any());
        assertEquals(mockUser.getEmail(), user.getEmail());
    }

}
