package com.rd.epm.springtesting.config.jpa;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories(value = "com.rd.epm.springtesting.repository")
public class JpaConfig {
}
