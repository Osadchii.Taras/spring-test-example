package com.rd.epm.springtesting.repository;

import com.rd.epm.springtesting.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, String> {
}
