package com.rd.epm.springtesting.service;

import com.rd.epm.springtesting.exception.UserNotFoundException;
import com.rd.epm.springtesting.model.User;
import com.rd.epm.springtesting.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;


    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public User findById(String id) {
        return userRepository.findById(id).orElseThrow(
                () -> new UserNotFoundException("User not found with such id: " + id)
        );
    }

    @Override
    public void save(User user) {
        userRepository.save(user);
    }
}
