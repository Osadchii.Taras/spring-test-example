package com.rd.epm.springtesting.service;

import com.rd.epm.springtesting.model.User;

import java.util.List;

public interface UserService {

    List<User> findAll();

    User findById(String id);

    void save(User user);
}
